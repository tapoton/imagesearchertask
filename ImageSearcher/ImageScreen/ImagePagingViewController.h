//
//  ImagePagingViewController.h
//  ImageSearcher
//
//  Created by Антон Тютин on 15.03.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePagingViewController : UIPageViewController 
@property (strong, nonatomic) NSArray *imageURLs;
@property (nonatomic) NSUInteger currentIndex;

@end
