//
//  ImagePagingViewController.m
//  ImageSearcher
//
//  Created by Антон Тютин on 15.03.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "ImagePagingViewController.h"
#import "ImageViewController.h"

@interface ImagePagingViewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@end

@implementation ImagePagingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.dataSource = self;
    self.delegate = self;
    
    [self setViewControllers:@[[self viewControllerAtIndex:self.currentIndex]]
                   direction:UIPageViewControllerNavigationDirectionForward
                    animated:NO
                  completion:nil];
}

- (ImageViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (index >= self.imageURLs.count) {
        return nil;
    }
    ImageViewController *controller = [[ImageViewController alloc] init];
    controller.imageURL = self.imageURLs[index];
    return controller;
}

- (NSUInteger)indexOfViewController:(UIViewController *)controller
{
    if ([controller isKindOfClass:[ImageViewController class]]) {
        NSUInteger index = [self.imageURLs indexOfObject:[(ImageViewController *)controller imageURL]];
        return index;
    }
    return NSNotFound;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger URLIndex = [self indexOfViewController:viewController];
    ImageViewController *controllerBefore = [self viewControllerAtIndex:URLIndex-1];
    return controllerBefore;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger URLIndex = [self indexOfViewController:viewController];
    ImageViewController *controllerBefore = [self viewControllerAtIndex:URLIndex+1];
    return controllerBefore;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (completed) {
        self.currentIndex = [self indexOfViewController:[self.viewControllers firstObject]];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
