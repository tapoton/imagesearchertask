//
//  ImageViewController.h
//  ImageSearcher
//
//  Created by Антон Тютин on 15.03.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController

@property (strong, nonatomic) NSURL *imageURL;

@end
