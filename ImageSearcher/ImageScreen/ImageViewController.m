//
//  ImageViewController.m
//  ImageSearcher
//
//  Created by Антон Тютин on 15.03.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "ImageViewController.h"
#import "ImageCollectionViewCell.h"
#import "UIKit+AFNetworking.h"

@implementation ImageViewController

- (void)loadView
{
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.userInteractionEnabled = YES;
    imageView.backgroundColor = [UIColor blackColor];
    self.view = imageView;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [(UIImageView *)self.view setImageWithURL:self.imageURL];
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                            action:@selector(didTap)];
    [tapGR setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tapGR];
}

- (void)didTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
