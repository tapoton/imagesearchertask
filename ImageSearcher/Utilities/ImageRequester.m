//
//  ImageRequester.m
//  ImageSearcher
//
//  Created by Антон Тютин on 14.03.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "ImageRequester.h"

@interface ImageRequester ()

@end

@implementation ImageRequester

- (instancetype)init
{
    self = [super init];
    if (self) {
        _canMakeRequest = YES;
    }
    return self;
}

- (void)didMakeRequest
{
    self.canMakeRequest = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.canMakeRequest = YES;
        [self callDeferredRequest];
    });
}

- (void)requestImagesForQuiery:(NSString *)quieryString fromRange:(NSRange)range completion:(void (^)(NSArray *))completion
{
    NSLog(@"Base class method does not need to be called (%s)", __PRETTY_FUNCTION__);
}

- (void)callDeferredRequest
{
    NSLog(@"Base class method does not need to be called (%s)", __PRETTY_FUNCTION__);
}

@end
