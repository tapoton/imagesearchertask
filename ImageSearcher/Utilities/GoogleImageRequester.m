//
//  GoogleImageRequester.m
//  ImageSearcher
//
//  Created by Антон Тютин on 14.03.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "GoogleImageRequester.h"
#import "AFNetworking.h"
#import "ReachabilityManager.h"

@interface GoogleImageRequester ()

@property (strong, nonatomic) AFURLSessionManager *sessionManager;
@end

@implementation GoogleImageRequester

- (AFURLSessionManager *)sessionManager
{
    if (!_sessionManager) {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _sessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    }
    return _sessionManager;
}

- (void)requestImagesForQuiery:(NSString *)quieryString fromRange:(NSRange)range completion:(void (^)(NSArray *))completion
{
    if (![ReachabilityManager checkReachability]) {
        completion(nil);
        return;
    }
    NSString *urlString = @"http://ajax.googleapis.com/ajax/services/search/images";
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET"
                                                                          URLString:urlString
                                                                         parameters:@{@"v" : @"1.0",
                                                                                      @"q" : quieryString,
                                                                                      @"start" : @(range.location),
                                                                                      @"rsz" : @(range.length > 8 ? 8 : range.length)}
                                                                              error:nil];
    NSURLSessionDataTask *dataTask = [self.sessionManager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response,
                                                                                                          id responseObject,
                                                                                                          NSError *error) {
        NSMutableArray *URLs = [NSMutableArray array];
        if (!responseObject) {
            NSLog(@"No response object for request: %@", request);
            completion(URLs);
            return;
        }
        NSDictionary *responseData = [responseObject objectForKey:@"responseData"];
        if ([responseData isEqual:[NSNull null]]) {
            NSLog(@"No results are given for request: %@", request);
            completion(URLs);
            return;
        }
        NSArray *results = responseData[@"results"];
        NSUInteger offset = 0;
        for (NSDictionary *imageDescription in results) {
            NSString *stringURL = imageDescription[@"url"];
            if (stringURL) {
                [URLs addObject:[NSURL URLWithString:stringURL]];
                offset++;
            }
        }
        
        completion(URLs);
    }];
    if (!self.canMakeRequest) {
        NSLog(@"Requests are too frequent. Deferring request");
        self.deferredRequestObject = dataTask;
    } else {
        [dataTask resume];
        [self didMakeRequest];
    }
}

- (void)callDeferredRequest
{
    if (self.deferredRequestObject) {
        [(NSURLSessionDataTask *)self.deferredRequestObject resume];
        [self didMakeRequest];
    }
}

@end
