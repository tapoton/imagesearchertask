//
//  ImageRequester.h
//  ImageSearcher
//
//  Created by Антон Тютин on 14.03.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageRequester : NSObject

@property (nonatomic) BOOL canMakeRequest;
@property (strong, nonatomic) id deferredRequestObject;

- (void)requestImagesForQuiery:(NSString *)quieryString
                     fromRange:(NSRange)range
                    completion:(void(^)(NSArray *URLs))completion;

- (void)callDeferredRequest;
- (void)didMakeRequest;
@end
