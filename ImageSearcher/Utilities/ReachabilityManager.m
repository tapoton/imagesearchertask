//
//  ReachabilityManager.m
//  ImageSearcher
//
//  Created by Антон Тютин on 15.03.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "ReachabilityManager.h"
#import "AFNetworking.h"

@implementation ReachabilityManager

static BOOL didShowAlertRecently = NO;

+ (BOOL)checkReachability
{
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        if (!didShowAlertRecently) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Connection error", nil)
                                                                message:NSLocalizedString(@"The internet connection is missing", nil)
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            didShowAlertRecently = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                didShowAlertRecently = NO;
            });
        }
        return NO;
    }
    return YES;
}

@end
