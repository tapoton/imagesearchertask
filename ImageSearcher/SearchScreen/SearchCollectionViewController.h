//
//  SearchCollectionViewController.h
//  ImageSearcher
//
//  Created by Антон Тютин on 14.03.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCollectionViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@end
