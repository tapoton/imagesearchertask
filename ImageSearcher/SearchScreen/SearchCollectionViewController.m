//
//  SearchCollectionViewController.m
//  ImageSearcher
//
//  Created by Антон Тютин on 14.03.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "SearchCollectionViewController.h"
#import "ImageCollectionViewCell.h"
#import "ImagePagingViewController.h"

#import "SVPullToRefresh.h"
#import "UIKit+AFNetworking.h"
#import "GoogleImageRequester.h"

static NSString *const TipMessage = @"Type your quiery to the search field";
static NSString *const NoResultsMessage = @"No results were found for quiery";

@interface SearchCollectionViewController () <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UISearchBarDelegate>

@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) NSString *searchString;
@property (strong, nonatomic) GoogleImageRequester *imageRequester;
@end

@implementation SearchCollectionViewController

- (GoogleImageRequester *)imageRequester
{
    if (!_imageRequester) {
        _imageRequester = [[GoogleImageRequester alloc] init];
    }
    return _imageRequester;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    __weak typeof(self) weakSelf = self;
    [self.collectionView addInfiniteScrollingWithActionHandler:^{
        [weakSelf addImages];
    }];
    self.messageLabel.text = NSLocalizedString(TipMessage, nil);
    self.messageLabel.hidden = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#define IMAGES_ON_PAGE 8

- (void)addImages
{
    NSUInteger start = self.dataSource.count;
    __weak typeof(self) weakSelf = self;
    [self.imageRequester requestImagesForQuiery:self.searchString fromRange:NSMakeRange(start, IMAGES_ON_PAGE) completion:^(NSArray *URLs) {
        if (![URLs count]) {
            [weakSelf.collectionView.infiniteScrollingView stopAnimating];
            return;
        }
        [weakSelf.dataSource addObjectsFromArray:URLs];
        [weakSelf.collectionView performBatchUpdates:^{
            NSMutableArray *indexPaths = [NSMutableArray array];
            for (NSUInteger i = start; i < start+URLs.count; i++) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
                [indexPaths addObject:indexPath];
            }
            [weakSelf.collectionView insertItemsAtIndexPaths:indexPaths];
        } completion:^(BOOL finished) {
            [weakSelf.collectionView.infiniteScrollingView stopAnimating];
        }];
    }];
}

- (void)requestImages
{
    __weak typeof(self) weakSelf = self;
    [self.imageRequester requestImagesForQuiery:self.searchString fromRange:NSMakeRange(0, IMAGES_ON_PAGE) completion:^(NSArray *URLs) {
        weakSelf.dataSource = nil;
        if (URLs && URLs.count == 0) {
            if (self.searchString.length) {
                self.messageLabel.text = NSLocalizedString(NoResultsMessage, nil);
            } else {
                self.messageLabel.text = NSLocalizedString(TipMessage, nil);
            }
            self.messageLabel.hidden = NO;
        } else {
            self.messageLabel.hidden = YES;
        }
        [weakSelf.dataSource addObjectsFromArray:URLs];
        [weakSelf.collectionView reloadData];
    }];
}

#pragma mark - Search bar delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.searchString = searchBar.text;
    [self requestImages];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar endEditing:YES];
}

#pragma mark - Collection view data source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    NSURL *imageURL = self.dataSource[indexPath.item];
    [cell.imageView setImageWithURL:imageURL
                   placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    return cell;
}

#pragma mark - Collection view delegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"FullscreenImageSegue"]) {
        ImagePagingViewController *controller = segue.destinationViewController;
        controller.imageURLs = self.dataSource;
        NSIndexPath *selectedIndexPath = [[self.collectionView indexPathsForSelectedItems] firstObject];
        controller.currentIndex = selectedIndexPath.item;
    }
}
@end
